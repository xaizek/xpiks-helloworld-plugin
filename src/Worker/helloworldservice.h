/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef HELLOWORLDSERVICE_H
#define HELLOWORLDSERVICE_H

#include <memory>

#include <QObject>
#include <QString>

#include <Common/flags.h>

namespace Artworks {
    class IArtworkMetadata;
}

class HelloWorldWorker;

class HelloWorldService: public QObject
{
    Q_OBJECT
public:
    explicit HelloWorldService(QObject *parent = nullptr);

public:
    void startService();
    void stopService();

    bool isAvailable() const { return m_IsAvailable; }
    bool isBusy() const { return false; }

    void submitItem(std::shared_ptr<Artworks::IArtworkMetadata> const &item);
    void submitItem(std::shared_ptr<Artworks::IArtworkMetadata> const &item, Common::WarningsCheckFlags flags);

public:
    void disableService() { m_IsAvailable = false; }
    void enableService() { m_IsAvailable = true; }

public slots:
    void disableRequested() { disableService(); }

private slots:
    void workerDestroyed(QObject *object);

private:
    HelloWorldWorker *m_Worker;
    bool m_IsAvailable;
};

#endif // HELLOWORLDSERVICE_H

/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#include "helloworldservice.h"

#include <QDebug>
#include <QEventLoop>
#include <QThread>
#include <QtGlobal>
#include <QTimer>

#include "Worker/helloworkercommand.h"
#include "Worker/helloworldworker.h"

HelloWorldService::HelloWorldService(QObject *parent) :
    QObject(parent),
    m_Worker(nullptr)
{
}

void HelloWorldService::startService() {
    if (m_Worker != nullptr) { return; }

    m_Worker = new HelloWorldWorker();

    QThread *thread = new QThread();
    m_Worker->moveToThread(thread);

    QObject::connect(thread, &QThread::started, m_Worker, &HelloWorldWorker::process);
    QObject::connect(m_Worker, &HelloWorldWorker::stopped, thread, &QThread::quit);

    QObject::connect(m_Worker, &HelloWorldWorker::stopped, m_Worker, &HelloWorldWorker::deleteLater);
    QObject::connect(thread, &QThread::finished, thread, &QThread::deleteLater);

    QObject::connect(m_Worker, &HelloWorldWorker::destroyed,
                     this, &HelloWorldService::workerDestroyed);

    qInfo() << "Starting worker";

    thread->start();
}

void HelloWorldService::stopService() {
    if (m_Worker != nullptr) {
        QEventLoop loop;

        QTimer timeoutTimer;
        timeoutTimer.setSingleShot(true);
        QObject::connect(&timeoutTimer, &QTimer::timeout, &loop, &QEventLoop::quit);
        QObject::connect(m_Worker, &HelloWorldWorker::stopped, &loop, &QEventLoop::quit);

        timeoutTimer.start(1000);

        qInfo() << "Stopping worker...";
        m_Worker->stopWorking();

        loop.exec();
    }
}

void HelloWorldService::submitItem(std::shared_ptr<Artworks::IArtworkMetadata> const &item) {
    Common::WarningsCheckFlags defaultFlags = Common::WarningsCheckFlags::All;
    this->submitItem(item, defaultFlags);
}

void HelloWorldService::submitItem(std::shared_ptr<Artworks::IArtworkMetadata> const &item,
                                   Common::WarningsCheckFlags flags) {
    if (m_Worker == nullptr) { return; }

    auto command = std::make_shared<HelloWorkerCommand>(item, flags);
    m_Worker->submitItem(command);
}

void HelloWorldService::workerDestroyed(QObject *object) {
    Q_UNUSED(object);
    m_Worker = nullptr;
}

/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef HELLOWORLDMODEL_H
#define HELLOWORLDMODEL_H

#include <QObject>
#include <QString>

class HelloWorldModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString greetingText READ getGreetingText WRITE setGreetingText NOTIFY greetingTextChanged)
public:
    explicit HelloWorldModel(QObject *parent = nullptr);

public:
    const QString &getGreetingText() const { return m_GreetingText; }
    void setGreetingText(const QString &value) {
        if (m_GreetingText != value) {
            m_GreetingText = value;
            emit greetingTextChanged();
        }
    }

signals:
    void greetingTextChanged();

private:
    QString m_GreetingText;
};

#endif // HELLOWORLDMODEL_H

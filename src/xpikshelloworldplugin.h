/*
 * This file is a part of Xpiks - cross platform application for
 * keywording and uploading images for microstocks
 * Copyright (C) 2014-2020 Taras Kushnir <tk.dev@mailbox.org>
 */

#ifndef XPIKSHELLOWORLDPLUGIN_H
#define XPIKSHELLOWORLDPLUGIN_H

#include <memory>
#include <vector>

#include <QHash>
#include <QObject>
#include <QString>
#include <QVariant>

#include <Common/flags.h>
#include <Plugins/xpiksplugininterface.h>

#include "Model/helloworldmodel.h"
#include "Worker/helloworldservice.h"

namespace Commands {
    class ICommandManager;
}

namespace Common {
    class ISystemEnvironment;
}

namespace KeywordsPresets {
    class IPresetsManager;
}

namespace Models {
    class ICurrentEditableSource;
}

namespace Plugins {
    class IPluginAction;
    class IUIProvider;
}

class XpiksHelloworldPlugin :
        public QObject,
        public Plugins::XpiksPluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "Xpiks.Plugins.XpiksPluginInterface.v0.1" FILE "helloworld.json")
    Q_INTERFACES(Plugins::XpiksPluginInterface)

public:
    XpiksHelloworldPlugin(QObject *parent=nullptr);
    virtual ~XpiksHelloworldPlugin();

    // XpiksPluginInterface interface
public:
    virtual QString getPrettyName() const override;
    virtual QString getVersionString() const override;
    virtual QString getAuthor() const override;

public:
    virtual std::vector<std::shared_ptr<Plugins::IPluginAction> > getExportedActions() const override;
    virtual bool executeAction(int actionID) override;

public:
    virtual bool initialize(Common::ISystemEnvironment &environment) override;
    virtual void finalize() override;
    virtual void enable() override;
    virtual void disable() override;

public:
    virtual Common::PluginNotificationFlags getDesiredNotificationFlags() const override;
    virtual void onPropertyChanged(Common::PluginNotificationFlags flag, const QVariant &data, void *pointer) override;

public:
    virtual void injectCommandManager(Commands::ICommandManager *commandManager) override;
    virtual void injectUIProvider(Plugins::IUIProvider *uiProvider) override;
    virtual void injectPresetsManager(KeywordsPresets::IPresetsManager *presetsManager) override;
    virtual void injectCurrentEditable(Models::ICurrentEditableSource *currentEditableSource) override;

private:
    int m_InsertedTabID;

    Commands::ICommandManager *m_CommandManager;
    Plugins::IUIProvider *m_UIProvider;
    KeywordsPresets::IPresetsManager *m_PresetsManager;
    Models::ICurrentEditableSource *m_CurrentEditableSource;

    HelloWorldService m_HelloWorldService;
    HelloWorldModel m_HelloWorldModel;
};

#endif // XPIKSHELLOWORLDPLUGIN_H

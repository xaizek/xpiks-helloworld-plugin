#include "makeuserhappyaction.h"

namespace Actions {
    int MakeUserHappyAction::s_ActionID = 123;

    MakeUserHappyAction::MakeUserHappyAction(QObject *parent):
        QObject(parent)
    {
    }

    QString MakeUserHappyAction::getActionName() const {
        return QObject::tr("Make user happy");
    }

    int MakeUserHappyAction::getActionID() const {
        return s_ActionID;
    }
}
